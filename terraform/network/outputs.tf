output "vpc_id" {
  value = aws_vpc.taz.id
}

output "public_subnet_id" {
  value = aws_subnet.taz.id
}
