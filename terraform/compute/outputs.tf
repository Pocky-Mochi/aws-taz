output "ssh_config" {
  value =  <<EOF
Host taz
  Hostname ${aws_eip.taz.public_ip}

Host *
  IdentityFile keys/ansible.key
  User admin
  IdentitiesOnly yes
EOF
}